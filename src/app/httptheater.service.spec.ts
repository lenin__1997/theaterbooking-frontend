import { TestBed } from '@angular/core/testing';

import { HttptheaterService } from './httptheater.service';

describe('HttptheaterService', () => {
  let service: HttptheaterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttptheaterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
