export class Validation {
    username?: String;
    password?: String;
    email?: String;
    role?: String;
    experience?: String;
    dob?: Date;
}
