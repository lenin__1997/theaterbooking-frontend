import { Component } from '@angular/core';
import { User } from '../user';
import { HttptheaterService } from '../httptheater.service';
import * as Chart from 'chart.js';
import { Spent } from '../spent';
// import { NgChartjsModule } from 'ng-chartjs';



@Component({
  selector: 'app-userchart',
  templateUrl: './userchart.component.html',
  styleUrls: ['./userchart.component.css']
})
export class UserchartComponent {

  spent : Spent[] = [];
  user : User[] = [];
  

  constructor(private httptheaterService: HttptheaterService){}
  i:any

  ngOnInit(){
    this.httptheaterService.getUserSpent().subscribe(data=>this.spent=data);
    this.httptheaterService.getUser().subscribe((data)=>{
        this.user=data;
        this.piechart();
      });    
  }

  chart:any
  piechart(){
      let username = [];
      let amount = [];
      for (let i = 0; i < this.user.length; i++) {
        username.push(this.user[i].user);
        // console.log(username)
      }
      for(let i=0;i<this.spent.length;i++){
        amount.push(this.spent[i].amount);
        // username.push(this.spent[i].customerId);
      }

    console.log(username)
      this.chart=new Chart('newchart',{
        type:'line',
        data:{
           labels:username,
          datasets:[{
            label:"Amount Spent per Day",
            data:amount,
            backgroundColor:['red','blue',"green","violet"],
            borderWidth:1,
          }]
        },
        options:{
          aspectRatio:5.5
      
        }
      });
    
  
  }
  
}
