import { Component, Output ,EventEmitter,Input } from '@angular/core';
import { HttptheaterService } from '../httptheater.service';
import { Router } from '@angular/router';
import { Films } from '../films';
import { MatDialog } from '@angular/material/dialog';
import { FilmbookComponent } from '../filmbook/filmbook.component';
import { User } from '../user';


@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent {

  
  films:Films[]=[];
  filter = {
    keyword:'',
    sortBy:"Name"
  }
  filmname:any;
  filmbook:any;
  user : any;
  username:any;
  useremail:any;
  
  constructor(private httptheaterService: HttptheaterService,private router:Router,public matDialog:MatDialog){}
  
  ngOnInit(){
    this.httptheaterService.getFilms().subscribe(data=>{this.films=data});
  }
  

  movielist(){
    this.httptheaterService.getFilms().subscribe(
      data=>this.films= this.filterExpenses(data)
      )
  }
  
  filterExpenses(films: Films[]){
    return films.filter((e)=>{
      return e.films.toLowerCase().includes(this.filter.keyword.toLowerCase());
    }).sort((a,b) => {
      if(this.filter.sortBy ==='Name'){
        return a.films.toLowerCase() < b.films.toLowerCase() ? -1 : 1 ;
      }
      else{
        return a.price < b.price ? -1 : 1;
      }
    })
  }
  
  popup(id:number){
    this.filmbook=this.films[id-1];
    this.httptheaterService.send(this.filmbook)
    this.matDialog.open(FilmbookComponent,{
      data: {
        id: 2
            },
      width:'45%',height:'95%'});

  }

}
