import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './registration/registration.component';
import { FilmsComponent } from './films/films.component';
import { UserchartComponent } from './userchart/userchart.component';
import { FilmbookComponent } from './filmbook/filmbook.component';

const routes: Routes = [
  {path:"admin",component:AdminComponent},
  {path:"user",component:UserComponent},
  {path:"registration",component:RegistrationComponent},
  {path:"films",component:FilmsComponent},
  {path:"userchart",component:UserchartComponent},
  {path:"",component:UserComponent},
  {path:"filmbook/:id",component:FilmbookComponent},
  // {path:"filmbook",component:FilmbookComponent},
  {path:"**",component:UserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
