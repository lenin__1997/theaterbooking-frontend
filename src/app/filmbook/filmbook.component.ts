import { Component, Input } from '@angular/core';
import { Films } from '../films';
import { HttptheaterService } from '../httptheater.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Spent } from '../spent';
import { BookingDetails } from '../booking-details';

@Component({
  selector: 'app-filmbook',
  templateUrl: './filmbook.component.html',
  styleUrls: ['./filmbook.component.css']
})
export class FilmbookComponent {

  films : Films = new Films();
  spent : Spent = new Spent();
  seats:number=0;
  popcorn:number=0;
  user:any
  constructor(private httptheaterService: HttptheaterService,private router: Router,private activeRoute : ActivatedRoute,public matDialog:MatDialog){}

  ngOnInit():void{
    this.films= this.httptheaterService.recive()
    this.user= this.httptheaterService.reciverUser();
  } 
  moviemail:any
  bookNow(){
    console.log(this.films.films)
    console.log(this.user.email)

    // this.httptheaterService.sendMail({
    //   email: this.user.email,
    //   movie: this.films.films 
    // }).subscribe(data=>this.moviemail=data)
    
  }
  click(){
    
    console.log(this.user.email);
  }
  // let email="";
  
  total!:number
  visible:boolean = false
  seatsValue(event:any){
    this.seats=event.target.value;
    this.visible = !this.visible
    this.total=((this.seats)*(this.films.price)) +this.popcorn;
    console.log(this.seats)
  }
  
  visible2:boolean = false
  addcorn(){
    this.popcorn=8
    this.total=((this.seats)*(this.films.price)) +this.popcorn;
    this.visible2 = !this.visible2
  }
  
  buynow(){
    console.log("Booked for This film ",this.films.films)
    this.total=((this.seats)*(this.films.price)) +this.popcorn;
    console.log(this.total)
    this.httptheaterService.sendMail({
      email: this.user.email,
      movie: this.films.films 
    }).subscribe(data=>this.moviemail=data)
    
    this.matDialog.closeAll();
    alert(this.films.films+" Movie Booked Successfully")
    return this.seats=0;
  }
  
  
 

  // To get film by router id
  // const isIdPresent = this.activeRoute.snapshot.paramMap.has('id');
  // if(isIdPresent){
  //   let id = Number(this.activeRoute.snapshot.paramMap.get('id'));
  //   this.httptheaterService.getFilmById(id).subscribe(
  //     data=>this.films=data)
  // }
}
