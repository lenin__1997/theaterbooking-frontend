import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmbookComponent } from './filmbook.component';

describe('FilmbookComponent', () => {
  let component: FilmbookComponent;
  let fixture: ComponentFixture<FilmbookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilmbookComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FilmbookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
