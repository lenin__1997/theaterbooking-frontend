import { Component } from '@angular/core';
import { HttptheaterService } from '../httptheater.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {
  
  login:any = {};
  
  constructor(private router: Router){}
  ngOnInit(){
    
  }
  loginUser(){
    var user = this.login.user;
    var password = this.login.password;
    if(user=="admin" && password=="admin"){
      this.router.navigateByUrl("/userchart")
      console.log("Loggin Success")
    }
    else{
      alert("Admin Loggin Failed Retry Again")
      console.log("Loggin Fail")
    }

  }
}
