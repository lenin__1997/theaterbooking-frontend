import { Component } from '@angular/core';
import { User } from '../user';
import { HttptheaterService } from '../httptheater.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Validation } from '../validation'

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {
  signupclassref: Validation = new Validation();

userForm: FormGroup = new FormGroup({

UserName: new FormControl('', [Validators.required]),

Password: new FormControl('', [Validators.required]),

 });
  user: User = new User();

  constructor(private httptheaterService: HttptheaterService,private router: Router){}
  
  saveUser(){
    this.httptheaterService.postUser(this.user).subscribe(data=>{
      this.router.navigateByUrl("/user");
      });
    }
  
  email = new FormControl('', [Validators.required, Validators.email]);
  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }
    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
}
