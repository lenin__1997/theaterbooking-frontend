import { Component } from '@angular/core';
import { Films } from './films';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular';


  films:Films[]=[]
  parentTxtValue: String ;
  parentClick:Subject<void> = new Subject<void>();
  childTextBoxValue: String | undefined;

  constructor(){
    this.parentTxtValue={} as String
  }
  onParentButtonClick() {
    this.parentClick.next();
  }

  updateInChildValue(event: any) {
    this.childTextBoxValue = event;

  }
}