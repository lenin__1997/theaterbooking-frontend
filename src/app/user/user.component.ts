import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttptheaterService } from '../httptheater.service';
import { User } from '../user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  username:any
  password:any
  getData:any;
  user: User=new User();
  useremail: any
  
  constructor(private httpservice:HttptheaterService,private router: Router){}
  ngOnInit(){ 
    
  }

  loginUser(){
    var user = this.username;
    var password = this.password;
    
    this.httpservice.getUserLoggin(user,password).subscribe((res:any)=>{
      this.getData=res;
      console.log(this.getData.user)
      console.log(this.getData)
    },error=>{
      alert("invalid")
    });
    
    if(user===this.getData.user && password===this.getData.password){
      console.log("success")
      this.router.navigate(["/films"])
      this.httpservice.sendUserEmail(this.getData)
    }
    else{
      alert("invalid")
    }
  }


  
}
