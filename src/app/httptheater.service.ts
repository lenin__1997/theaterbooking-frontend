import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable,map } from 'rxjs';
import { User } from './user';
import { Films } from './films';
import { Spent } from './spent';
import { TypeModifier } from '@angular/compiler';
import { BookingDetails } from './booking-details';



@Injectable({
  providedIn: 'root'
})
export class HttptheaterService {

  constructor(private httpClient:HttpClient) { }

  user:any;
  email:any;
  sendUserEmail(user:any){
    this.user=user;
    // this.email=email;
  }
  reciverUser(){
    return this.user;
    // ,this.email;
  }

  // Transfer data from film component to filmbook component
  id:any
  filmbook:any;
  sendid(id: any){
    this.id=id;
  }
  reciveid(){
    return this.id;
  }
  send(name:any){
    this.filmbook=name;
  }
  recive(){
    return this.filmbook;
  }
  // User
  getUser():Observable<User[]>{
    return this.httpClient.get<User[]>('http://localhost:8080/user');
  }
  postUser(user:User):Observable<User>{
    return this.httpClient.post<User>('http://localhost:8080/user',user);
  }
  getUserLoggin(user:String,password:String):Observable<User>{
    return this.httpClient.get<User>('http://localhost:8080/userlogin/' +user+ '/' +password);
  }
  // getUserEmail(user1:String){
  //   return this.httpClient.get('http://localhost:8080/user/' + user1);
  // }
  // Films
  getFilms():Observable<Films[]>{
    return this.httpClient.get<Films[]>('http://localhost:8080/films');
  }
  getFilmById(id:number):Observable<Films>{
    return this.httpClient.get<Films>(`${'http://localhost:8080/films'}/${id}`);
  }
  // Spent
  getUserSpent():Observable<Spent[]>{
    return this.httpClient.get<Spent[]>('http://localhost:8080/spent');
  }

  postUserSpent(spent:Spent):Observable<Spent>{
    return this.httpClient.post<Spent>('http://localhost:8080/spent',spent);
  }

  sendMail(bookingDetails:BookingDetails):Observable<BookingDetails>{
    return this.httpClient.post<BookingDetails>('http://localhost:8080/buy',bookingDetails);
  }


}
